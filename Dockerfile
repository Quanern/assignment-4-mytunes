FROM openjdk:15
ADD target/assignment-4-myTunes-0.0.1-SNAPSHOT.jar quan-renze-assignment4-mytunes.jar
ENTRYPOINT ["java", "-jar", "/quan-renze-assignment4-mytunes.jar"]