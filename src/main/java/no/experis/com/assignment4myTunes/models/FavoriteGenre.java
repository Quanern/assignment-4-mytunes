package no.experis.com.assignment4myTunes.models;

public class FavoriteGenre {
    private int amount;
    private String genre;

    /**
     * Simple POJO to store information about genres and favorite numbers.
     * @param amount - Number of songs in this genre
     * @param genre - Name of the genre
     */
    public FavoriteGenre(int amount, String genre) {
        this.amount = amount;
        this.genre = genre;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
