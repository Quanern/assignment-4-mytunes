package no.experis.com.assignment4myTunes.models;

public class SearchResult {
    private String trackName;
    private String artistName;
    private String albumTitle;
    private String genre;
    private String trackID;

    /**
     * Simple POJO to store search results.
     * @param trackName - Name of the track.
     * @param artistName - The artist who made the track.
     * @param albumTitle - The album which the track is a part of.
     * @param genre - Name of the genre of the track.
     */
    public SearchResult(String trackID, String trackName, String artistName, String albumTitle, String genre) {
        this.trackID = trackID;
        this.trackName = trackName;
        this.artistName = artistName;
        this.albumTitle = albumTitle;
        this.genre = genre;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTrackID() {
        return trackID;
    }

    public void setTrackID(String trackID) {
        this.trackID = trackID;
    }
}
