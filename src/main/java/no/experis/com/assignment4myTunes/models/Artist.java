package no.experis.com.assignment4myTunes.models;

public class Artist {
    private String name;

    /**
     * Simple POJO to keep track of artist name.
     * @param name - Name of the artist.
     */
    public Artist(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
