package no.experis.com.assignment4myTunes.models;

public class Track {
    private String name;
    private String artistName;

    /**
     * Simple POJO to store track name and corresponding artist.
     * @param name - Name of the track
     * @param artistName - Name of the artist
     */
    public Track(String name, String artistName) {
        this.name = name;
        this.artistName = artistName;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}
