package no.experis.com.assignment4myTunes.models;

public class BiggestSpender {
    private int customerId;
    private String firstName;
    private String lastName;
    private double totalSpent;

    /**
     * A simple POJO class for biggest spenders.
     * @param customerId - ID of the customer in the database.
     * @param firstName - First name of the customer.
     * @param lastName - Last name of the customer.
     * @param totalSpent - Total amount the customer has spent in all invoices.
     */
    public BiggestSpender(int customerId, String firstName, String lastName, double totalSpent) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.totalSpent = totalSpent;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(double totalSpent) {
        this.totalSpent = totalSpent;
    }
}
