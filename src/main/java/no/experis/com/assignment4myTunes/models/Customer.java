package no.experis.com.assignment4myTunes.models;

public class Customer {
    private int customerId;
    private String firstName;
    private String lastName;
    private String country;
    private String postalCode;
    private String phone;
    private String email;

    /**
     * Simple POJO to keep track of customer information.
     */
    public Customer(){}

    /**
     * Simple POJO to keep track of customer information.
     * @param customerId - ID of the customer in the database.
     * @param firstName - First name of the customer.
     * @param lastName - Last name of the customer.
     * @param country - Country the customer resides in.
     * @param postalCode - Postal code of the customer.
     * @param phone - Phone number of the customer.
     * @param email - Email of the customer.
     */
    public Customer(int customerId, String firstName, String lastName, String country, String postalCode, String phone, String email) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
        this.email = email;
    }

    /**
     * Simple POJO to keep track of customer information. Used in adding new users to the database.
     * @param firstName - First name of the customer.
     * @param lastName - Last name of the customer.
     * @param country - Country the customer resides in.
     * @param postalCode - Postal code of the customer.
     * @param phone - Phone number of the customer.
     * @param email - Email of the customer.
     */
    public Customer(String firstName, String lastName, String country, String postalCode, String phone, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
        this.email = email;
    }


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
