package no.experis.com.assignment4myTunes.models;

public class Genre {
    private String name;

    /**
     * Simple POJO to store genre name.
     * @param name - Name of the genre.
     */
    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
