package no.experis.com.assignment4myTunes.models;

public class UsersByCountry {
    private int amountOfCustomers;
    private String country;

    /**
     * Simple POJO that stores information about the number of users in a country.
     * @param amountOfCustomers - Number of users.
     * @param country - Name of the country.
     */
    public UsersByCountry(int amountOfCustomers, String country) {
        this.amountOfCustomers = amountOfCustomers;
        this.country = country;
    }

    public int getAmountOfCustomers() {
        return amountOfCustomers;
    }

    public void setAmountOfCustomers(int amountOfCustomers) {
        this.amountOfCustomers = amountOfCustomers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
