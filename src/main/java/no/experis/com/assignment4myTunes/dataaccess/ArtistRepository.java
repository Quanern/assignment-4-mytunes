package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.Artist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
/**
 * This class contains methods that takes use of the model Artist.java
 */
public class ArtistRepository {
    private Connection conn = null; //Initialize the connection without starting it.

    /**
     * This method is used to get 5 random artists from the database.
     * @return An ArrayList containing 5 random Artists from the Artist table.
     */
    public ArrayList<Artist> selectFiveRandomArtists(){
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            // Opens the connection.
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare the statement, escaping it to avoid SQL-injection.
            // The statements SELECT the Names of the artists from the Artist Table, scramble the order they
            // come in and only takes the 5 first artists.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM artist " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 5");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            //Takes the names of the Artists to create a new Artist and add it to the ArrayList.
            while (resultSet.next()) {
                artists.add(
                        new Artist(
                                resultSet.getString("name")
                        ));
            }
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return artists;
    }
}
