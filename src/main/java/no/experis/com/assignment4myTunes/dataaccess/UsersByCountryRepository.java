package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.UsersByCountry;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * This class contains methods that takes use of the model UsersByCountry.java
 */
public class UsersByCountryRepository {

    private Connection conn = null;
    public ArrayList<UsersByCountry> getUsersByCountries(){
        ArrayList<UsersByCountry> usersByCountries = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT COUNT(CustomerId) AS Customers, Country " +
                            "FROM Customer GROUP BY Country ORDER BY Customers DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                usersByCountries.add(
                        new UsersByCountry(
                                resultSet.getInt("Customers"),
                                resultSet.getString("Country")
                        ));
            }


            // Process Results
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return usersByCountries;
    }
}
