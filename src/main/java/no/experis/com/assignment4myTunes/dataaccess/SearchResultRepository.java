package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.SearchResult;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * This class contains methods that takes use of the model SearchResult.java
 */
public class SearchResultRepository {
    private Connection conn = null;

    /**
     * This method is used to get results for when you search for a song.
     * @param searchWord The word you want to search for
     * @return ArrayList with the search results.
     */
    public ArrayList<SearchResult> selectAllSearchResult(String searchWord){
        ArrayList<SearchResult> tracks = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // This statement selects the trackId, track, artist, album and genre of the tracks that matches the search word.
            // This select goes through the table track, artist, album and genre. Artist join album on artistId,
            // album join track on albumid, and track join genre on genreid.
            // The LIKE is used to search for character sequence instead of having to equalling it to something.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT " +
                            "track.trackID AS TrackID," +
                            "track.name AS Name, " +
                            "artist.name AS Artist, " +
                            "album.Title AS Album, " +
                            "genre.Name AS Genre " +
                            "FROM track " +
                            "INNER JOIN artist ON artist.artistId = album.artistId " +
                            "INNER JOIN album ON album.AlbumId = track.AlbumId " +
                            "INNER JOIN genre ON genre.GenreId = track.GenreId " +
                            "WHERE track.name LIKE ?");
            // By setting a % before and after the search word makes it so the statement will match songs as long as the
            // sequence of the search word is in the track name.
            preparedStatement.setString(1, "%"+searchWord+"%");//Corresponds to the first, and only, ?.
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(
                        new SearchResult(
                                resultSet.getString("TrackID"),
                                resultSet.getString("Name"),
                                resultSet.getString("Artist"),
                                resultSet.getString("Album"),
                                resultSet.getString("Genre")
                        ));
            }


            // Process Results
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return tracks;
    }

    /**
     * This method is used to get a specific result by the
     * @param trackID ID of the track you want to select
     * @return SearchResult with the search result.
     */
    public SearchResult selectSpecificSearchResult(String trackID){
        SearchResult searchResult = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // This statement selects the trackId, track, artist, album and genre of the tracks for the given trackId.
            // This select goes through the table track, artist, album and genre. Artist join album on artistId,
            // album join track on albumid, and track join genre on genreid.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT " +
                            "track.trackID AS TrackID," +
                            "track.name AS Name, " +
                            "artist.name AS Artist, " +
                            "album.Title AS Album, " +
                            "genre.Name AS Genre " +
                            "FROM track " +
                            "INNER JOIN artist ON artist.artistId = album.artistId " +
                            "INNER JOIN album ON album.AlbumId = track.AlbumId " +
                            "INNER JOIN genre ON genre.GenreId = track.GenreId " +
                            "WHERE track.trackid = ?");
            preparedStatement.setString(1, trackID);//Corresponds to the first, and only, ?.
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                searchResult = new SearchResult(
                        resultSet.getString("TrackID"),
                        resultSet.getString("Name"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre")
                );
            }


            // Process Results
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return searchResult;
    }
}
