package no.experis.com.assignment4myTunes.dataaccess;
/**
 * This class is used to set up the connection. Will be moved to a .config file.
 */
class ConnectionHelper {
    static final String CONNECTION_URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
}
