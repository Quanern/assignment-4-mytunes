package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.Customer;

import java.sql.*;
import java.util.ArrayList;

/**
 * This class contains methods that takes use of the model Customer.java
 */
public class CustomerRepository {
    // Initializes the connection without opening it.
    private Connection conn =null;

    /**
     * This method is used to get all the customers in the database.
     * @return an ArrayList of all the Customers in the database.
     */
    public ArrayList<Customer> selectAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // This statements Selects the id, name, country, postalcode, phone and email from the customer table.
            // This will return a list of all customers with those columns.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId AS \"Customer ID\", FirstName AS \"First Name\", " +
                            "LastName AS \"Last Name\", Country, PostalCode, Phone, Email FROM Customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Takes the id, names, country, postalCode, phone and email to create a new
            // Customer and add it to the ArrayList.
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getString(6),
                                resultSet.getString(7)
                        ));
            }
        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return customers;
    }

    /**
     * This method returns the highest id of the customers in the database.
     * It is not to be used by itself since it never opens or closes a connection.
     * @return an integer with the highest id a customer has.
     */
    private int maxCustomerId(){
        int maxId = 0;
        try {

            //Prepare Statement
            //Selects the highest customerId from customer.
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT MAX(customerId) FROM customer");

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();
            maxId = resultSet.getInt("MAX(customerId)");

        }
        catch (Exception ex){
            System.out.println("Something went wrong:\n" + ex.toString());
        }

        return maxId;
    }

    /**
     * This method is used to create a new Customer and set the new customer's customerId.
     * @param customer The customer that you want to add to the database.
     * @return the customerId that has been given to the customer so you can check what the customerId is for
     * future references.
     */
    public Customer addNewCustomer(Customer customer){
        //The new id to be given to the customer.
        int newId;
        boolean success = false;
        try {

            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");
            newId = maxCustomerId() + 1; //Gets the highest customerId in the database and adds it with 1.
            customer.setCustomerId(newId); //Sets the customer's customerId.

            // Prepare Statement
            // This statements takes the customer's customerId, first and last name, country, postalcode, phone
            // and email and puts it into the database.
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO customer " +
                    "VALUES (?,?,?,'','','','',?,?,?,'',?,'')");
            preparedStatement.setInt(1, newId);//Corresponds to the first ?.
            preparedStatement.setString(2, customer.getFirstName());//Corresponds to the second ?.
            preparedStatement.setString(3, customer.getLastName());//Corresponds to the third ?.
            preparedStatement.setString(4, customer.getCountry());//Corresponds to the fourth ?.
            preparedStatement.setString(5, customer.getPostalCode());//Corresponds to the fifth ?.
            preparedStatement.setString(6, customer.getPhone());//Corresponds to the sixth ?.
            preparedStatement.setString(7, customer.getEmail());//Corresponds to the seventh ?.

            //Execute query
            preparedStatement.executeUpdate();
            success = true;
        }
        catch (Exception ex){
            System.out.println("Something went wrong:\n" + ex.toString());
        }
        finally{
            try {
                conn.close();
            } catch (SQLException ex){
                System.out.println("Something went wrong while closing the connection:\n" + ex);
            }
        }
        if(!success){
            return null;
        }
        return customer;
    }

    /**
     * This method is used to update an existing customer.
     * @param updatedCustomer the new information you want to be updated in the database.
     * @return the new and updated customer.
     */
    public Customer updateExistingCustomer(Customer updatedCustomer){
        Customer customer;
        boolean success = false;
        try {
            // Open connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            //Prepare Statement
            //This statement takes the information to be updated from the customer and puts it into the database.
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE customer SET FirstName=?, " +
                    "LastName=?, Country =?, PostalCode=?, Phone=?, Email=? WHERE customerId=?");
            preparedStatement.setString(1, updatedCustomer.getFirstName());//Corresponds to the first ?.
            preparedStatement.setString(2, updatedCustomer.getLastName());//Corresponds to the second ?.
            preparedStatement.setString(3, updatedCustomer.getCountry());//Corresponds to the third ?.
            preparedStatement.setString(4, updatedCustomer.getPostalCode());//Corresponds to the fourth ?.
            preparedStatement.setString(5, updatedCustomer.getPhone());//Corresponds to the fifth ?.
            preparedStatement.setString(6, updatedCustomer.getEmail());//Corresponds to the sixth ?.
            preparedStatement.setInt(7, updatedCustomer.getCustomerId());//Corresponds to the seventh ?.


            //Execute query
            preparedStatement.executeUpdate();
            success = true;
        }
        catch (Exception ex){
            System.out.println("Something went wrong:\n" + ex.toString());
        }
        finally{
            try {
                conn.close();
            } catch (SQLException ex){
                System.out.println("Something went wrong while closing the connection:\n" + ex);
            }
        }
        // Opens a new connection to get the updated customer.
        if(!success){
            return null;
        }
        customer = selectSpecificCustomer(updatedCustomer.getCustomerId());
        return customer;
    }


    /**
     * This method is used to find information about a specific customer.
     * @param customerId the customerId of the customer you want information about.
     * @return the customer you wanted information about.
     */
    public Customer selectSpecificCustomer(int customerId){
        Customer customer = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // This statement selects the name, country, postalCode, phone and email of the customer that you
            // searched for.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT FirstName AS \"First Name\", LastName AS \"Last Name\", " +
                            "Country, PostalCode, Phone, Email FROM customer WHERE customerId = ?");
            preparedStatement.setInt(1, customerId); //Corresponds to the first, and only, ?.
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Takes the id, names, country, postalCode, phone and email to create a new
            // Customer and add it to the ArrayList.
            while (resultSet.next()) {
                customer = new Customer(
                        customerId,
                        resultSet.getString("First Name"),
                        resultSet.getString("Last Name"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return customer;
    }

}
