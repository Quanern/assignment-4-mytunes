package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * This class contains methods that takes use of the model Track.java
 */
public class TrackRepository {
    private Connection conn = null;

    /**
     * This method is used to get five random tracks and their artists.
     * @return ArrayList containing the 5 random tracks.
     */
    public ArrayList<Track> selectFiveRandomTracks(){
        ArrayList<Track> tracks = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // This statement Selects the name of the track and it's artist, scrambles the order and returns the first
            // five. It has to join track on album and then album on artist to get both names since there is no direct
            // relation between Track and Artist in the database.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT t.name Track, ar.name Artist FROM track t " +
                            "JOIN Album al ON al.albumid = t.albumid " +
                            "JOIN Artist ar ON ar.artistid = al.artistid " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 5");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            //Takes the names of the track and artist to create a new Track and add it to the ArrayList.
            while (resultSet.next()) {
                tracks.add(
                        new Track(
                                resultSet.getString("Track"),
                                resultSet.getString("Artist")
                        ));
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return tracks;
    }
}
