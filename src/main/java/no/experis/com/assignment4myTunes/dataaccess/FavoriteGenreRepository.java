package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.FavoriteGenre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * This class contains methods that takes use of the model FavoriteGenre.java
 */
public class FavoriteGenreRepository {

    private Connection conn = null;

    /**
     * This method is used to get a specific customer's favorite genre.
     * @param customerId The id of the customer you want to get favorite genre for.
     * @return ArrayList of the customer's favorite genres.
     */
    public ArrayList<FavoriteGenre> selectFavoriteGenre(int customerId){
        ArrayList<FavoriteGenre> spenders = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // This statement first creates a new temporary table called temp. The table returns what genres the
            // customer has bought and how many of them. This table is a JOIN of the tables invoice, invoiceline,
            // track and genre. Invoice and invoiceline join on invoiceid, track and invoiceline join on trackid,
            // track and genre join on genreid.
            // The second Select will get the amount bought and the genre
            // from temp where the amount is the highest. This makes it so if a customer has bought the same amount
            // in two genres, both of them will show up.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("WITH temp AS " +
                            "(SELECT COUNT(i.invoiceId) Amount, " +
                            "g.name Name " +
                            "FROM invoice i " +
                            "JOIN invoiceline il " +
                            "ON il.invoiceid = i.invoiceid " +
                            "JOIN track t " +
                            "ON t.trackId = il.trackId " +
                            "JOIN genre g " +
                            "ON g.genreId = t.genreId " +
                            "WHERE i.customerid = ? " +
                            "GROUP BY  g.name " +
                            "ORDER BY  Amount desc) " +
                            "SELECT Amount, Name " +
                            "FROM temp " +
                            "WHERE Amount = (SELECT MAX(Amount) FROM temp)");
            preparedStatement.setInt(1, customerId); //Corresponds to the first, and only, ?.
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            //Takes the amount bought and name of the genre to create a new FavoriteGenre and add it to the ArrayList.
            while (resultSet.next()) {
                spenders.add(
                        new FavoriteGenre(
                                resultSet.getInt("Amount"),
                                resultSet.getString("Name")

                        ));
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return spenders;
    }
}
