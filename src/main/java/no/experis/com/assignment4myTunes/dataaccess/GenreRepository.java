package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.Genre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * This class contains methods that takes use of the model Genre.java
 */
public class GenreRepository {
    private Connection conn = null;

    /**
     * This method is used to get five random genres.
     * @return ArrayList containing 5 different genres.
     */
    public ArrayList<Genre> selectFiveRandomGenres(){
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // This statement Selects the name From the table genre and scrambles their order. It will then limit it
            // with the 5 first ones only.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM genre " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 5");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            //Takes the name of the genre to create a new Genre and add it to the ArrayList.
            while (resultSet.next()) {
                genres.add(
                        new Genre(
                                resultSet.getString("name")
                        ));
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return genres;
    }
}
