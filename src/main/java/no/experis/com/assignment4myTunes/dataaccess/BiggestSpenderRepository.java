package no.experis.com.assignment4myTunes.dataaccess;

import no.experis.com.assignment4myTunes.models.BiggestSpender;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * This class contains methods that takes use of the model BiggestSpender.java
 */
public class BiggestSpenderRepository {
    private Connection conn = null;

    /**
     * This method is used to get the customers who has spent the most money on myTunes.
     * @return ArrayList with the id, name and total amount spent by the users, with the one spending the most
     * at the top.
     */
    public ArrayList<BiggestSpender> selectBiggestSpenders(){
        ArrayList<BiggestSpender> spenders = new ArrayList<>();
        try {
            // Open the connection
            conn = DriverManager.getConnection(ConnectionHelper.CONNECTION_URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            // The statement takes the customerId, First Name, Last Name and Total money spent from the temp table
            // Made from joining customer and invoice, where the customerId is the relation.
            // The temp table Sums up the invoices of each customer and groups them up by the customerId;
            // The first Select will then Order the results by the temp table's Total, having the highest first.
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT temp.customerId AS ID, temp.firstname AS \"First Name\", " +
                            "temp.lastname AS \"Last Name\" , temp.Total AS Total " +
                            "FROM (SELECT c.customerId, firstname, lastname, SUM(i.total) Total " +
                            "FROM customer c " +
                            "JOIN invoice i ON  c.customerId = i.customerId " +
                            "GROUP BY c.customerId) temp " +
                            "ORDER BY temp.Total DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            //Takes the Id, First Name, Last Name and Total to create a new BiggestSpender and add it to the ArrayList.
            while (resultSet.next()) {
                spenders.add(
                        new BiggestSpender(
                                resultSet.getInt("ID"),
                                resultSet.getString("First Name"),
                                resultSet.getString("Last Name"),
                                resultSet.getDouble("Total")
                        ));
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        }
        finally {
            try {
                // Close Connection
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return spenders;
    }
}
