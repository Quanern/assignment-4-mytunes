package no.experis.com.assignment4myTunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment4MyTunesApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment4MyTunesApplication.class, args);
	}

}
