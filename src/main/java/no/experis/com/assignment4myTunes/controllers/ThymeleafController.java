package no.experis.com.assignment4myTunes.controllers;

import no.experis.com.assignment4myTunes.dataaccess.ArtistRepository;
import no.experis.com.assignment4myTunes.dataaccess.GenreRepository;
import no.experis.com.assignment4myTunes.dataaccess.SearchResultRepository;
import no.experis.com.assignment4myTunes.dataaccess.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class ThymeleafController {

    private ArtistRepository artistRepository = new ArtistRepository();
    private TrackRepository trackRepository = new TrackRepository();
    private GenreRepository genreRepository = new GenreRepository();
    private SearchResultRepository searchResultRepository = new SearchResultRepository();


    /**
     * Method to get the 5 random songs, artists and genres on the front page.
     * @param model the thymeleaf model used to show information.
     * @return the html file in the templates folder in resources.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homePage(Model model) {
        model.addAttribute("songs", trackRepository.selectFiveRandomTracks());
        model.addAttribute("artists", artistRepository.selectFiveRandomArtists());
        model.addAttribute("genres", genreRepository.selectFiveRandomGenres());
        return "home";
    }

    /**
     * Method to get search results after a user has searched for a song.
     * @param model the thymeleaf model used to show information.
     * @return the html file in the templates folder in resources.
     */
    @RequestMapping(value = "/track", method = RequestMethod.GET)
    public String searchResult(@RequestParam("search") String search, Model model){
        model.addAttribute("search", search);
        model.addAttribute("searchResults", searchResultRepository.selectAllSearchResult(search));
        return "search-result-view";
    }

    /**
     * Method to get the details about a specific track after the user has searched for a song
     * @param model the thymeleaf model used to show information.
     * @return the html file in the templates folder in resources.
     */
    @RequestMapping(value="/track/{id}", method = RequestMethod.GET)
    public String showTrackDetails(@PathVariable String id, Model model) {
        System.out.println(id);
        model.addAttribute("searchResult", searchResultRepository.selectSpecificSearchResult(id));
        return "view-track-details";
    }
}
