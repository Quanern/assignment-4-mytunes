package no.experis.com.assignment4myTunes.controllers;

import no.experis.com.assignment4myTunes.dataaccess.BiggestSpenderRepository;
import no.experis.com.assignment4myTunes.dataaccess.CustomerRepository;
import no.experis.com.assignment4myTunes.dataaccess.FavoriteGenreRepository;
import no.experis.com.assignment4myTunes.dataaccess.UsersByCountryRepository;
import no.experis.com.assignment4myTunes.models.BiggestSpender;
import no.experis.com.assignment4myTunes.models.Customer;
import no.experis.com.assignment4myTunes.models.FavoriteGenre;
import no.experis.com.assignment4myTunes.models.UsersByCountry;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    private FavoriteGenreRepository favoriteGenreRepository = new FavoriteGenreRepository();
    private BiggestSpenderRepository biggestSpenderRepository = new BiggestSpenderRepository();
    private UsersByCountryRepository usersByCountryRepository = new UsersByCountryRepository();
    private CustomerRepository customerRepository = new CustomerRepository();

    /**
     * Returns a JSON file with all the customer in the database.
     */
    @GetMapping("/api/customers")
    @ResponseBody
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.selectAllCustomers();
    }


    /**
     * Creates a customer in the database, then returns the customerID when finished.
     */
    @PostMapping("/api/customers")
    public Customer createCustomer(@RequestBody Customer customer) {
        return customerRepository.addNewCustomer(customer);
    }

    /**
     * Returns JSON file containing information about the customer with given CustomerID
     */
    @GetMapping("/api/customers/{id}")
    @ResponseBody
    public Customer getSpecificCustomer(@PathVariable(value = "id") int customerID){
        return customerRepository.selectSpecificCustomer(customerID);
    }

    /**
     * Updates a customer with the given id, then returns the updated customer.
     */
    @PutMapping("/api/customers/{id}")
    public Customer updateCustomer(@PathVariable(value = "id") Long customerID,
                                 @RequestBody Customer customer) {
        return customerRepository.updateExistingCustomer(customer);
    }

    /**
     * Returns a JSON file with the number of customers in each country.
     */
    @GetMapping("api/customers/country")
    @ResponseBody
    public ArrayList<UsersByCountry> getCustomersByCountry() {
        return usersByCountryRepository.getUsersByCountries();
    }

    /**
     * Returns a JSON file with the highest spenders, ordered descending.
     */
    @GetMapping("api/customers/spending")
    @ResponseBody
    public ArrayList<BiggestSpender> getBiggestSpenders(){
        return biggestSpenderRepository.selectBiggestSpenders();
    }

    /**
     * Returns a JSON file with the provided user's most popular genre.
     */
    @GetMapping("api/customers/{id}/popular/genre")
    @ResponseBody
    public ArrayList<FavoriteGenre> getCustomersFavoriteGenre (@PathVariable int id){
        return favoriteGenreRepository.selectFavoriteGenre(id);
    }
}
