# Assignment 4 - myTunes
MyTunes is a web application that displays information about a service that is similar to Itunes. All the data used 
is taken from the SQLite database "Chinook". It contains two parts: a simple web interface built using Thymeleaf which 
displays various data and an api which responds with JSON-objects.

Link to the web application: https://quan-renze-assignment4-mytunes.herokuapp.com/

Link to the layout of the database: https://www.codestencil.com/database/chinook-database-schema

## Web interface
The web interface uses Thymeleaf templates to display information to the user. 
It can also take input from the user and react accordingly.
The default page displays five random tracks (their name and their artist), five random artists, and five random genres 
from the database. It also has a search bar at the top of the page where the user can search for tracks. 
This search bar is case-insensitive. When the user search, they are taking to a new page (located at /track?search=)
which displays all the tracks with the search-text in their title. More information about the tracks is also displayed 
here, including artist, genre and album. Furthermore, each track has a button which takes you to their details page 
(located at /tracks/{id}). The same information is displayed here, but it is open to be extended in the future.

## API
The API uses RestController from the Spring framework to handle requests. There is also a JSON-file with a collection
of Postman requests to the endpoints listed below in the postman folder in the project.

### Postman
If you don't have Postman installed, download the installer from https://www.postman.com/downloads/ or use the 
web-app https://web.postman.co/. You will need a user.

To get the Postman collection, open your Postman, and on the left side there is an import button. Click on the import 
button and drag the JSON-file in the postman folder into the window. A new window will pop up and click on import in
the bottom right. You will now have imported the API requests.

### api/customers
This api call has both a Get and Post implementation. 
When using Get, it returns a JSON object with a list of all the customers in the database with their: customer ID, 
first name, last name, country, postal code, phone number and email. When using Post, it takes a JSON object 
containing the same customer data, and then creates a new customer in the database.

### api/customers/{id}
This api call has both a Get and Put implementation. 
When using Get, it returns a JSON object with the following information for the specified customer: 
customer ID, first name, last name, country, postal code and email. 
When using Put, it takes a JSON object with the same format and updates the corresponding customer
in the database with the provided data.

### api/customers/{id}/popular/genre
This api call has a Get implementation.
It returns a JSON object with the genre the specified user has the most tracks from, 
and the number of tracks the customer has bought in that genre. 

### api/customers/country
This api call has a Get implementation.
It returns a JSON object with a list of countries and the number of users in that country. 

### api/customers/spending
This api call has a Get implementation.
It returns a JSON object with a list of all customers, ordered by the biggest spendors. 
It includes the following information per customer: customer ID, first name, last name and total amount spent.

## Contributors
Quan Tran

Renze Stolte